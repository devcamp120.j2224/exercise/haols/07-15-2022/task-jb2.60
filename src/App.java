import com.devcamp.s50.taskjbr260.Animal;
import com.devcamp.s50.taskjbr260.Cat;
import com.devcamp.s50.taskjbr260.Dog;
import com.devcamp.s50.taskjbr260.Mammal;

public class App {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal("Lucky");
        Animal animal2 = new Animal("Bobby");
        System.out.println(animal1.toString());
        System.out.println(animal2.toString());

        Mammal mammal1 = new Mammal("Bust");
        Mammal mammal2 = new Mammal("Lock");
        System.out.println(mammal1.toString());
        System.out.println(mammal2.toString());

        Cat cat1 = new Cat("Queen");
        Cat cat2 = new Cat("King");
        cat1.greets();
        System.out.println(cat1.toString());
        cat2.greets();
        System.out.println(cat2.toString());

        Dog Dog1 = new Dog("Buck");
        Dog Dog2 = new Dog("Nicky");
        System.out.println(Dog1.toString());
        Dog1.greets();
        Dog1.greets(Dog2);

        System.out.println(Dog2.toString());
        Dog2.greets();

    }
}
